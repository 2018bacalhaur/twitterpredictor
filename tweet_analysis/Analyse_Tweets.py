import numpy as np
from tweet_collection.to_dataframe import transform_to_dataframe
from tweet_collection.twitter_users import collect_by_user

tweets = collect_by_user("EmmanuelMacron")
data = transform_to_dataframe(tweets)

rt_max  = np.max(data['RTs'])
rt  = data[data.RTs == rt_max].index[0]

# Max RTs:
print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
print("Number of retweets: {}".format(rt_max))

print ("")
#Max Likes:

likes_max  = np.max(data['likes'])
likes  = data[data.likes == likes_max].index[0]

print("The tweet with more likes is: \n{}".format(data['tweet_textual_content'][likes]))
print("Number of likes: {}".format(likes_max))

