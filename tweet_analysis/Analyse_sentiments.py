from textblob import TextBlob

from tweet_collection.twitter_users import collect_by_user


def extrat_words():
    tw_text=[]
    text_blob = ''
    text_temp = ''
    word_lemmatize=[]

    #Take a test for a list
    list_tweets_status = collect_by_user("EmmanuelMacron")

    for tweet_status in list_tweets_status:
        tw_text.append(tweet_status.text)



    #Transform the all text into TextBlob

    for text in tw_text:
        text_temp = text_temp + text

    text_blob = TextBlob(text_temp)

    for word in text_blob.word:
        words= Word(word)
        word_lemmatize.append(words.lemmateze)

    words_lemmatize_set = set(word_lemmatize)

    return words_lemmatize_set



#Analyse of sentiments

def sentiment_analysis(twitter_canditate):
    tw_text = []
    tweets_posit = []
    tweets_negat = []
    tweets_neutral = []

    list_tweets_status = collect_by_user(twitter_canditate)


    for tweets_status in list_tweets_status:
        tw_text.append(tweets_status.text)

    for text in tw_text:
        sentiments = TextBlob(text).sentiment.polarity

        if sentiments > 0:
            tweets_posit.append(text)

        elif sentiments < 0:
            tweets_negat.append(text)

        else:
            tweets_neutral.append(text)

    a = len(tweets_posit)*100/(len(tweets_posit)+len(tweets_negat)+len(tweets_neutral))
    b = len(tweets_neutral)*100/(len(tweets_posit)+len(tweets_negat)+len(tweets_neutral))
    c = len(tweets_negat)*100/(len(tweets_posit)+len(tweets_negat)+len(tweets_neutral))

    return print("Percentage of positive tweets: {}%" "\n" "Percentage of neutral tweets: {}%" "\n" "Percentage de negative tweets: {}%".format(a,b,c))


sentiment_analysis("EmmanuelMacron")

