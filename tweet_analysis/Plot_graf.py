import pandas as pd
from tweet_collection.to_dataframe import transform_to_dataframe
from tweet_collection.twitter_users import collect_by_user
import matplotlib.pyplot as plt


tweets = collect_by_user("EmmanuelMacron")
data = transform_to_dataframe(tweets)


tfav = pd.Series(data['likes'].values, index=data['Date'])
tret = pd.Series(data['RTs'].values, index=data['Date'])

# Likes vs retweets visualization:
tfav.plot(figsize=(16,4), label="Likes", legend=True)
tret.plot(figsize=(16,4), label="Retweets", legend=True)

plt.show()
