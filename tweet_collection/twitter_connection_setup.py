import tweepy
from tweet_collection.credentials import *

def twitter_setup():

    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:

    api = tweepy.API(auth)
    return api

the_api = twitter_setup()
assert the_api.me()
