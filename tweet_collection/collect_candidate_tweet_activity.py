from tweet_collection.twitter_connection_setup import twitter_setup

def get_retweets_of_candidate(candidate_username):
    connection = twitter_setup()
    tweets = connection.user_timeline(screen_name=candidate_username, count=200)
    retweets_per_tweet = {}
    for status in tweets:
        # Only for original tweets, excluding the retweets
        if (not status.retweeted) or ('RT @' not in status.text):
            retweets_per_tweet[status.text] = status.retweet_count
    return retweets_per_tweet


