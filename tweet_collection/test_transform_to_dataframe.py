from tweet_collection.twitter_search import collect
from tweet_collection.to_dataframe import transform_to_dataframe
from pytest import *

def test_collect():

    search_term = 'EmmanuelMacron'
    tweets = collect(search_term)
    data = transform_to_dataframe(tweets)

    assert 'tweet_textual_content' in data.columns
