import pandas as pd
from tweet_collection.twitter_users import collect_by_user

def transform_to_dict(tweet):
    return {"tweet_textual_content": str(tweet.text),
            "len" : len(tweet.text),
            "RTs": tweet.retweet_count,
            "likes": tweet.favorite_count,
            "Date": tweet.created_at}

def transform_to_dataframe(tweets):
    list = []

    for i in tweets:
        liste.append(transform_to_dict(i))
    data_frame_ready = pd.DataFrame(list)

    return data_frame_ready


#tweets = collect_by_user("EmmanuelMacron")

#transform_to_dataframe(tweets)
