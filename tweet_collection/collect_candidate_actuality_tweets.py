import os
from tweet_collection.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError

def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    file_name_keywords = 'keywords_candidate' + str(num_candidate) + '.txt'
    file_name_hashtags = 'hashtag_candidate' + str(num_candidate) + '.txt'
    keywords = os.path.join(file_path, file_name_keywords)
    hashtags = os.path.join(file_path, file_name_hashtags)

    try:
        keywords = open(keywords).readlines()
        hashtags = open(hashtags).readlines()

        return keywords + hashtags

    except IOError:
        print('ERROR, Please try again')


def get_tweets_from_candidates_search_queries(queries, twitter_api):
    results = []
    try:

        for query in queries:
            query_result = twitter_api.search(query, language="french", rpp=100)
            results.append(query_result)
    except RateLimitError:

        print('An error occurred during the query, please check the API')

    return results



