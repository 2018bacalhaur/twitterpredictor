from tweet_collection.twitter_connection_setup import twitter_setup

def collect():

    connexion = twitter_setup()
    tweets = connexion.search("Emmanuel Macron", language="french", rpp=200)
    return tweets

